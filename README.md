# EEE3088F git repo pi-uHAT

pi-uHAT Uninterupted Power Supply (UPS) development
This pi-uHAT will be designed to supply power to a pi even when power is out in an area. It should ensure that a user has sufficient time to properly switch off the pi and other attached features if some components are sensitive to sudden power changes.

Consists of schematics and SPICE simulations of parts of the whole uHAT, a document breakdown of all electronic parts and their interfaces with eachother.
Files can be referenced for modification, but are only testing ground for the final build of the UPS device.
Feedback is welcome on possible short-falls of the existing Schematics.
